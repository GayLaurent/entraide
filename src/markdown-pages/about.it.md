---
title: "Chi siamo"
date: "2020-03-31T12:30:03.284Z"
---

_In seguito al confinamento causato dall’epidemia di COVID-19, molti di voi hanno bisogno di strumenti per continuare a comunicare con i propri cari e /o per il lavoro a distanza. Il collettivo di Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires - CHATONS – ha creato questo portale di accesso semplificato a dei servizi online senza registrazione e che rispondono alle esigenze più comuni._

Tutti questi servizi sono offerti dai provider CHATONS che si impegnano a rispettare il [manifesto del collettivo](https://chatons.org/en/charter). Avviato nel 2016 da Framasoft, questo collettivo riunisce oggi oltre 70 strutture che offrono servizi online liberi, etici, decentralizzati e solidali per consentire agli utenti di Internet di trovare rapidamente alternative ai servizi offerti dai giganti del web.

Abbiamo in programma di mantenere questa pagina oltre il tempo del confinamento e persino di arricchirla nelle settimane / mesi a venire per offrirvi a breve altri strumenti liberi proposti dagli CHATONS, compresi quelli che richiedono una registrazione.
