import React from "react"
import {Dropdown} from "react-bootstrap"
import {changeLocale, IntlContextConsumer, FormattedMessage} from "gatsby-plugin-intl";

const languageName = {
  de: "Deutsch",
  en: "English",
  fr: "Français",
  it: "Italiano",
  oc: "Occitan",
}

const Language = () => {
  return (
    <span>
      <Dropdown alignRight>
        <Dropdown.Toggle size="sm" variant="transparent" id="dropdown-basic">
          <FormattedMessage id="navbar.language" />
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <IntlContextConsumer>
            {({ languages, language: currentLocale }) =>
              languages.map(language => (
                <Dropdown.Item
                  key={language}
                  active={language === currentLocale}
                  onClick={() => changeLocale(language)}
                >
                  {languageName[language]}
                </Dropdown.Item>
              ))
            }
          </IntlContextConsumer>
          <Dropdown.Divider />
          <Dropdown.Item href="https://weblate.framasoft.org/projects/entraide-chatons/">
            <FormattedMessage id="navbar.translate" />
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </span>
  )
}

export default Language
