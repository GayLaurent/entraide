const { createFilePath } = require(`gatsby-source-filesystem`)
const path = require('path')

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages`, trailingSlash: false })
    const elems = slug.split('.')
    const lang = elems.pop()
    const slugWithoutLang = elems.join('.')
    createNodeField({
      node,
      name: `slug`,
      value: slugWithoutLang,
    })
    createNodeField({
      node,
      name: `language`,
      value: lang,
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {

  const {createPage} = actions
  const result = await graphql(`
        query {
          allMarkdownRemark {
            edges {
              node {
                fields {
                  slug,
                  language
                }
              }
            }
          }
        }
      `)

  result.data.allMarkdownRemark.edges.forEach(({node}) => {
    let slug = node.fields.slug
    createPage({
      path: slug,
      component: path.resolve(`./src/components/markdown_page.js`),
      context: {
        // Data passed to context is available
        // in page queries as GraphQL variables.
        slug: node.fields.slug,
        language: node.fields.language
      },
    })
  })
}

